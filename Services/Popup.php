<?php

namespace XLabs\PopupBundle\Services;

use Symfony\Component\HttpFoundation\RequestStack;

class Popup
{
    private $request;

    public function __construct(RequestStack $request_stack)
    {
        $this->request = $request_stack->getCurrentRequest();
    }

    public function show($type, $aMessage, $autoClose = false)
    {
        $message = is_array($aMessage) ? implode('<br />', $aMessage) : $aMessage;
        $this->request->getSession()->getFlashBag()->add('xlabs_'.$type, $message);
        if($autoClose)
        {
            if(is_bool($autoClose))
            {
                $this->request->getSession()->getFlashBag()->add('xlabs_autoclose_'.$type, 2500);
            } else {
                $this->request->getSession()->getFlashBag()->add('xlabs_autoclose_'.$type, $autoClose);
            }
        }
    }
}