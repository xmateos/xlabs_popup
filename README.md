Session popup user notifications.

## Installation ##

Install through composer:

```bash
php -d memory_limit=-1 composer.phar require xlabs/popupbundle
```

In your AppKernel

```php
public function registerbundles()
{
    return [
    	...
    	...
    	new XLabs\PopupBundle\XLabsPopupBundle(),
    ];
}
```

## Routing

Append to main routing file:

``` yml
# app/config/routing.yml
  
xlabs_popup:
    resource: "@XLabsPopupBundle/Resources/config/routing.yml"
    prefix:   /
```

### Usage ###
Append this at the very beginning of your body tag:
```php
{% include 'XLabsPopupBundle:Popup:loader.html.twig' %}
```

To see a sample template, check:
```php
XLabsPopupBundle:Popup:example.html.twig
```