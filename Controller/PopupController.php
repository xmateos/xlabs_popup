<?php

namespace XLabs\PopupBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PopupController extends Controller
{
    public function exampleAction()
    {
        $request = $this->get('request_stack')->getCurrentRequest();
        $type = $request->get('type');
        if($type)
        {
            $this->get('xlabs_popup')->show($type, array(
                'This is a test message 1',
                'This is a test message 2'
            ), true);
        }
        return $this->render('XLabsPopupBundle:Popup:example.html.twig');
    }
}
